title: utlogd: index
author: Eric Vidal <eric@obarun.org>

[Software](https://web.obarun.org/software)

[obarun.org](https://web.obarun.org)

# What is utlogd?

*Utlogd* is a login/logout user daemon tracker which use the inotify API to survey the change of the user by the utmp file. In function of the connection or disconnection of the user, *utlogd* launches all scripts set at the corresponding directory of an activated user—see [utlogctl](utlogctl.html).

It does nothing more, nothing less.

*Utlogd* does not depend on anything except a POSIX-compliant C development environment.

*Utlogd* does not interfer with a tracking user framework like Consolekit2 or Seatd and can be used together.

*Note*: GNU/Linux guarantees the existence of the utmp file but does not guarantee the readability of that file. In consequence, refer to the documentation of your distribution to be sure that your system have this file before trying to use this software.

utlogd works on mechanisms not on policies. It can be compiled with `glibc` or `musl`. For `musl` you will need utmps to build and run.

## Installation

### Requirements

Please refer to the [INSTALL.md](https://framagit.org/Obarun/utlogd) file for details.

### Licensing

*utlogd* is free software. It is available under the [ISC license](http://opensource.org/licenses/ISC).

### Upgrade

See [changes](upgrade.html) between version.

---

## Commands

- [utlogd](utlogd.html)
- [utlogctl](utlogctl.html)
