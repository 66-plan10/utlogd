#!/bin/sh

man1='utlogctl utlogd'

for i in 1 ;do
    if [ ! -d doc/man/man${i} ]; then
       mkdir -p -m 0755 doc/man/man"${i}" || exit 1
    fi
done

for i in ${man1}; do
    lowdown -s -Tman doc/"${i}".md -o doc/man/man1/"${i}".1 || exit 1
    var=$( sed -n -e '/^.TH/p' < doc/man/man1/"${i}".1)
    var=$(printf '%s' "$var" | tr '7' '1')
    sed -i "s!^.TH.*!${var}!" doc/man/man1/"${i}".1 || exit 1
    sed -i '4,8d' doc/man/man1/"${i}".1 || exit 1
done


exit 0
