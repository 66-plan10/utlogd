title: utlogd: utlogctl
author: Eric Vidal <eric@obarun.org>

[utlogd](index.html)

[Software](https://web.obarun.org/software)

[obarun.org](https://web.obarun.org)

# utlogctl

This command handles a script launched at user login/logout process tracked by the [utlogd](utlogd.html) daemon.

## Interface

```
    utlogctl [ -h ] [ -v verbosity ] [ -n ] [ -s ] [ -d ] [ -l ] user|login|logout|skelin|skelout username scriptname
```

Activate/deactivate a user or a user's scripts which will be launched at user login/logout process tracked by the [utlogd](utlogd.html) daemon. By default, a series of scripts present at `%%script_path%%/.skel/log{in,out}` (A.K.A skeleton scripts) directories are automatically copied to the corresponding `%%script_path%%/<username>/log{in,out}` directories at activation process.

## Exit codes

- *0* success
- *100* wrong usage
- *111* system call failed

## Actions

To do the reverse of the following actions, use the **-d** option.

In case of **login**, **logout**, **skelin** and **skelout** action, *scriptname* must be an absolute path. At contrary, if the **-d** option is used to deactivate a script, the path of the *scriptname* must be forgotten.

In case of **skelin** and **skelout** action, the argument *username* is replaced by the argument *scriptname*.

- **user** : activate the user's *username*.

- **login** : activate the user's script *scriptname* for the user's *username* at `%%script_path%%/<username>/login` directory.

- **logout** : activate the user's script *scriptname* for the user's *username* at `%%script_path%%/<username>/logout` directory.

- **skelin** : activate the script *scriptname* at `%%script_path%%/.skel/login` directory.

- **skelout** : activate the script *scriptname* at `%%script_path%%/.skel/logout` directory.

## Options

- **-h** : prints this help.

- **-v** *verbosity* : increases/decreases the verbosity of the command.
    * *1* : only prints error messages. This is the default.
    * *2* : also prints warning messages.
    * *3* : also prints tracing messages.
    * *4* : also prints debugging messages.

- **-n** : do not copy the skeleton files. This option only effects *username* activation. The corresponding `%%script_path%%/<username>/log{in,out}` is created but left empty.

- **-s** : creates a symlink pointing to *scriptname* at the corresponding `login,logout,skelin,skelout` directories instead of making a copy of the *scriptname*.

- **-d** : deactivates a user, a user's script or a skeleton script. In case of `user` actions, the corresponding `%%script_path%%/<username>/` is removed.

- **-l** : lists the contents of the corresponding `login,logout,skelin,skelout` directories depending on the actions passed. If the `user` action is passed, it lists the `%%script_path%%/<username>/log{in,out}` user directories.

## Limits

To optimize the memory resources used by the utlogd daemon, some limitations are applied:

- Maximum user on the system cannot exceed 256.
- The total characters of a script name cannot exceed 512.  This also means that the skeleton, login or logout directory cannot contain a number of scripts where the addition of their names exceed 512 character.

## Skeleton files and directories

By default, the skeleton files and directories are empty. It's the responsibility of the system administrator to provide it.

## Usage examples

Activate a user:

```
    utlogctl user oblive
```

Deactivate a user:

```
    utlogctl -d user oblive

```

Activate a user without copying the contents of the skeleton directory:

```
    utlogctl -n user oblive
```

Activate a script for a user login time:

```
    utlogctl login oblive /home/oblive/my_script.sh
```

Deactivate a script for a user login time:

```
    utlogctl -d login oblive my_script.sh
```

Activate a script for a user login time creating a symlink at the `%%script_path%%/login/` directory pointing to the location of the script instead of making a copy:

```
    utlogctl -s login oblive /home/oblive/my_script.sh
```

Activate a new skeleton script for the logout time:

```
    utlogctl skelout /usr/share/utlogd/new_script.sh
```

Deactivate a skeleton script for the logout time:

```
    utlogctl -d skelout new_script.sh
```

List the script activated for a user:

```
    utlogctl -l user obarun
```

List the script activated at the logout directory for a user:

```
    utlogctl -l logout obarun
```

List the skeleton script activated for the login time:

```
    utlogctl -l skelin
```
