title: utlogd: utlogd
author: Eric Vidal <eric@obarun.org>

[utlogd](index.html)

[Software](https://web.obarun.org/software)

[obarun.org](https://web.obarun.org)

# utlogd

An utmp login/logout user daemon tracker.

## Interface

```
    utlogd [ -h ] [ -v verbosity ] [ -d notif ]
```

*Utlogd* wait for event at the `%%utmp_path%%` file and react on this event by launching all scripts found at `%%script_path%%/<username>/login` user login and at `%%script_path%%/<username>/logout` user logout.

The user **needs** to first be activated by the intermediate of the [utlogctl](utlogctl.html) software. If the user is not activated, *utlogd* send a message to stderr and do nothing.

The default path for the utmp file and the main script directories can be changed at compile time by passing the `--utmp-path=DIR` and `--script-path=DIR` respectively to `./configure`.

This daemon is intended to be launched under a supervison suite like [s6](https://skarnet.org/software/s6/).

*Utlogd* **never** polls. It uses the inotify API to survey the `%%utmp_path%%` file.

If the utmp file is removed from the system while *utlogd* is running, it crashes and exits 111.

## Exit codes

- *0* success
- *100* wrong usage
- *111* system call failed

## Options

- **-h** : prints this help.

- **-v** *verbosity* : increases/decreases the verbosity of the command.
    * *1* : only prints error messages. This is the default.
    * *2* : also prints warning messages.
    * *3* : also prints tracing messages.
    * *4* : also prints debugging messages.

- **-d** *notif* : notify readiness on file descriptor *notif*. The notification happens right before the launch of the inotify API. This guarantees you that the *utlogd* is completely configured and ready to use.

## Scripts execution

The scripts **must be** executables.

Scripts launched by *utlogd* at a user event, is executed with the ownership of these files. It tries to use the setuid and setgid system call with the corresponding uid and gid of the script file. If that process fails, it sends a message to the stderr, ignore the script and passes to the next script to launch.

If the scripts cannot be launched for any reason, *utlogd* sends a message to stderr and passes to the next script to launch.

Before launching the scripts, *utlogd* sets the environment variable `USER` to the currently handled user.
