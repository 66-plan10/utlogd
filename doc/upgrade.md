title: utlogd: upgrade
author: Eric Vidal <eric@obarun.org>

[utlogd](index.html)

[Software](https://web.obarun.org/software)

[obarun.org](https://web.obarun.org)

# Changelog for utlogd

---

# In 0.0.1.0

First release
