![GitLabl Build Status](https://framagit.org/Obarun/66/badges/master/pipeline.svg)

Utlogd - Utmp login/logout user daemon tracker
===

*Utlogd* is a login/logout user daemon tracker which use the inotify API to survey the change of the user by the utmp file. In function of the connection or disconnection of the user, the *utlogd* launches all scripts set for the corresponding directory of an activated user—see [utlogctl](https://web.obarun.org/software/utlogd/utlogctl.html).

It does nothing more, nothing less.

*Utlogd* does not depend on anything else except a POSIX-compliant C development environment.

*Utlogd* does not interfer with a tracking user framework like Consolekit2 or Seatd and can be used together.

*Note*: GNU/Linux guarantee the existence of the utmp file but does not guarantee the readability of that file. In consequence, refer to the documentation of your distribution to make sure that your system has this file before trying to use this software.

utlogd works on mechanisms not on policies. It can be compiled with `glibc` or `musl`. For `musl` you will need utmps to build and run.

Installation
------------

See the INSTALL.md file.

Documentation
-------------

Online [documentation](https://web.obarun.org/software/utlogd)

Contact information
-------------------

* Email:
  Eric Vidal `<eric@obarun.org>`

* Web site:
  https://web.obarun.org/

* XMPP Channel:
    obarun@xmpp.obarun.org

Supports the project
---------------------

Please consider making a [donation](https://web.obarun.org/index.php?id=18)
