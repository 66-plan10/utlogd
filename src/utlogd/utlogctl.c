#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>//O_XXX
#include <errno.h>
#include <stdio.h>

#include <utlogd/config.h>
#include <utlogd/ulog.h>
#include <utlogd/utools.h>
#include <utlogd/constants.h>

#define USAGE "utlogctl [ -h ] [ -v verbosity ] [ -n ] [ -s ] [ -d ] [ -l ] user|login|logout|skelin|skelout username scriptname"

char const *help_utlogctl =
"utlogctl [ -h ] [ -v verbosity ] [ -n ] [ -s ] [ -d ] [ -l ] user|login|logout|skelin|skelout username scriptname"
"\n"
"options :\n"
"   -h: print this help\n"
"   -v: increase/decrease verbosity\n"
"   -n: do not copy skeleton file\n"
"   -s: symlink a file instead of making a copy\n"
"   -d: deactivate instead of activate\n"
"   -l: list script\n"
;

char *PROG = 0 ;
uint8_t SKEL = 1 ;
uint8_t SYMLINK = 0 ;
uint8_t ACTIVATE = 1 ;
uint8_t LIST = 0 ;

static inline uint8_t lookup (char const *const *table, char const *cmd)
{
    uint8_t i = 0 ;

    for (; table[i] ; i++)
        if (!strcmp(cmd, table[i]))
            break ;

    return i ;
}

static inline uint8_t parse_cmd (char const *cmd)
{
    static char const *const cmd_table[] =
    {
        "user",
        "login",
        "logout",
        "skelin",
        "skelout",
        0
    } ;

  uint8_t i = lookup(cmd_table, cmd) ;

  if (!cmd_table[i])
      ulog_die(ULOG_EXIT_USER, USAGE) ;

  return i ;
}

void utlogctl_parse_cmd(int argc, char const *const *argv, uint8_t *what, uint8_t *narg)
{
    argc-- ;
    argv++ ;

    for (; *argv ; argv++, (*narg)++) {

        if (*argv[0] == '-') {

            switch (argv[0][1]) {

                case 'h' :

                    fprintf(stderr, help_utlogctl) ;
                    _exit(0) ;

                case 'v' :

                    if (argv[0][2]) {

                        if (argv[0][2] < 48 || argv[0][2] > 57)
                            ulog_die(ULOG_EXIT_USER, "invalid value for verbosity option") ;

                        ULOG_VERBOSITY = strtol(*argv + 2, 0, 10) ;

                    } else {

                        argv++, (*narg)++ ;

                        if (!*argv)
                            ulog_die(ULOG_EXIT_USER,"missing value for verbosity") ;

                        if (*argv[0] < 48 || *argv[0] > 57)
                            ulog_die(ULOG_EXIT_USER, "invalid value for verbosity option") ;

                        ULOG_VERBOSITY = strtol(*argv, 0, 10);

                    }
                    break ;

                case 'd' :

                    ACTIVATE = 0 ;

                    break ;

                case 'n' :

                    SKEL = 0 ;

                    break ;

                case 's' :

                    SYMLINK = 1 ;

                    break ;

                case 'l' :

                    LIST = 1 ;

                    break ;

                default:
                    ulog_die(ULOG_EXIT_USER, USAGE) ;
            }

        } else {
            break ;
        }
    }

    if (*argv)
        (*what) = parse_cmd(*argv) ;
    else
        ulog_die(ULOG_EXIT_USER, USAGE) ;

    (*narg) += 2 ;
}

void utlogctl_skel_set_arraypath(char *array, int onoff)
{
    char *oo = (onoff == LOGIN) ?  SCRIPT_LOGIN : SCRIPT_LOGOUT ;
    size_t oolen = (onoff == LOGIN) ? SCRIPT_LOGIN_LEN : SCRIPT_LOGOUT_LEN ;

    memcpy(array, UTLOGD_SCRIPT_PATH SCRIPT_SKEL,UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN) ;
    memcpy(array + UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN, oo, oolen) ;
    array[UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + oolen] = '/' ;
}

void utlogctl_sanitize_user(char const *user)
{
    uid_t uid ;
    gid_t gid ;
    size_t userlen = strlen(user) ;

    char dst[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + SCRIPT_LOGOUT_LEN + 1] ;

    if (utools_get_uidbyname(user,&uid) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s", "unable to get uid of: ",user) ;

    if (utools_get_gidbyname(user,&gid) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s", "unable to get gid of: ",user) ;

    utools_user_set_arraypath(dst,user,LOGIN) ;

    dst[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + SCRIPT_LOGIN_LEN] = 0 ;

    ulog_f_trace("%s%s%s%lo","sanitize directory: ", dst," with mode: ", (unsigned long)0700) ;

    if (!utools_dir_create_parent(dst,0700))
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s","unable to create directory: ", dst) ;

    ulog_f_trace("%s%s%s%i:%i","chown: ", dst," with: ", (unsigned int)uid, (unsigned int) gid) ;

    if (chown(dst, uid, gid) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s", "unable to chown: ",dst) ;

    utools_user_set_arraypath(dst,user,LOGOUT) ;

    dst[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + SCRIPT_LOGOUT_LEN] = 0 ;

    ulog_f_trace("%s%s%s%lo","sanitize directory: ", dst," with mode: ", (unsigned long)0700) ;

    if (!utools_dir_create_parent(dst,0700))
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s","unable to create directory: ", dst) ;

    ulog_f_trace("%s%s%s%i:%i","chown: ", dst," with: ", (unsigned int)uid, (unsigned int) gid) ;

    if (chown(dst, uid, gid) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s", "unable to chown: ",dst) ;

    dst[UTLOGD_SCRIPT_PATH_LEN + userlen] = 0 ;

    ulog_f_trace("%s%s%s%i:%i","chown: ", dst," with: ", (unsigned int)uid, (unsigned int) gid) ;

    if (chown(dst, uid, gid) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s", "unable to chown: ",dst) ;
}



void utlogctl_skel_get_script(strchain_t *cdir, int onoff)
{
    size_t oolen = (onoff == LOGIN) ? SCRIPT_LOGIN_LEN : SCRIPT_LOGOUT_LEN ;
    char dir[UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + oolen + 1] ;

    utlogctl_skel_set_arraypath(dir,onoff) ;

    dir[UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + oolen] = 0 ;

    if (!utools_get_dir(dir ,cdir))
        ulog_f_die(ULOG_EXIT_SYS, "%s%s","unable to get file from: ",dir) ;

}

void utlogctl_skel_copy_script(strchain_t *cdir, char const *user, int onoff)
{
    size_t i = 0 ;
    size_t oolen = (onoff == LOGIN) ? SCRIPT_LOGIN_LEN : SCRIPT_LOGOUT_LEN ;
    size_t userlen = strlen(user) ;

    for ( ; i < cdir->len ; i += strlen(cdir->s + i) + 1) {

        size_t clen = strlen(cdir->s + i) ;
        size_t srclen = UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + oolen + 1 + clen ;
        char script[srclen + 1] ;
        char dst[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen + 1 + clen + 1] ;

        utlogctl_skel_set_arraypath(script,onoff) ;

        memcpy(script + UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + oolen + 1, cdir->s + i, clen) ;
        script[UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + oolen + 1 + clen] = 0 ;

        utools_user_set_arraypath(dst,user,onoff) ;

        memcpy(dst + UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen + 1, cdir->s + i, clen) ;
        dst[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen + 1 + clen] = 0 ;

        ulog_f_trace("%s%s%s%s", "copy: ",script, " to: ", dst) ;

        if (!utools_filecopy(script,dst))
            ulog_f_diesys(ULOG_EXIT_SYS,"%s%s%s%s","unable to copy: ",script," to: ",dst) ;
    }
}

void utlogctl_skel_copy(char const *user)
{
    strchain_t cdir ;

    utools_sc_init(&cdir) ;

    utlogctl_skel_get_script(&cdir, LOGIN) ;

    utlogctl_skel_copy_script(&cdir, user, LOGIN) ;

    utools_sc_init(&cdir) ;

    utlogctl_skel_get_script(&cdir, LOGOUT) ;

    utlogctl_skel_copy_script(&cdir, user, LOGOUT) ;

}

void utlogctl_user_add(char const *user)
{
    utlogctl_sanitize_user(user) ;

    if (SKEL)
        utlogctl_skel_copy(user) ;

}

void utlogctl_user_remove_script(char const *script, char const *user, int onoff)
{
    size_t scriptlen = strlen(script) ;
    size_t oolen = (onoff == LOGIN) ? SCRIPT_LOGIN_LEN : SCRIPT_LOGOUT_LEN ;
    size_t userlen = strlen(user) ;

    char file[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen + 1 + scriptlen + 1] ;

    utools_user_set_arraypath(file,user,onoff) ;

    memcpy(file + UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen + 1, script, scriptlen) ;
    file[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen + 1 + scriptlen] = 0 ;

    ulog_f_trace("%s%s", "remove file: ", file) ;

    if (unlink(file) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s","unable to remove: ",file) ;
}

void utlogctl_user_del(char const *user)
{
    strchain_t cdir ;

    size_t userlen = strlen(user), i = 0 ;
    char dir[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + SCRIPT_LOGOUT_LEN + 1] ;

    utools_user_set_arraypath(dir,user,LOGIN) ;
    dir[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + SCRIPT_LOGIN_LEN] = 0 ;

    if (access(dir,F_OK) < 0)
        return ;

    utools_sc_init(&cdir) ;

    utools_user_get_script(&cdir, user, LOGIN) ;

    for ( ; i < cdir.len ; i += strlen(cdir.s + i) + 1)

        utlogctl_user_remove_script(cdir.s + i, user, LOGIN) ;

    ulog_f_trace("%s%s", "remove directory: ", dir) ;

    if (rmdir(dir) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s","unable to remove: ",dir) ;

    utools_sc_init(&cdir) ;

    utools_user_get_script(&cdir, user, LOGOUT) ;

    for (i = 0 ; i < cdir.len ; i += strlen(cdir.s + i) + 1)

        utlogctl_user_remove_script(cdir.s + i, user, LOGOUT) ;

    utools_user_set_arraypath(dir,user,LOGOUT) ;

    dir[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + SCRIPT_LOGOUT_LEN] = 0 ;

    ulog_f_trace("%s%s", "remove directory: ", dir) ;

    if (rmdir(dir) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s","unable to remove: ",dir) ;

    dir[UTLOGD_SCRIPT_PATH_LEN + userlen] = 0 ;

    ulog_f_trace("%s%s", "remove directory: ", dir) ;

    if (rmdir(dir) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s","unable to remove: ",dir) ;

}

void utlogctl_list_user_script(char const *user, int onoff)
{
    size_t i = 0 ;
    strchain_t cdir ;

    utools_sc_init(&cdir) ;

    utools_user_get_script(&cdir,user,onoff) ;

    fprintf(stderr,"%s%s",onoff == LOGIN ? "login" : "logout"," script: ") ;

    for ( ; i < cdir.len ; i += strlen(cdir.s + i) + 1)

        fprintf(stderr,"%s ", cdir.s + i) ;

    fprintf(stderr,"%s","\n") ;
}

void utlogctl_list_skel_script(int onoff)
{

    size_t i = 0 ;
    strchain_t cdir ;

    utools_sc_init(&cdir) ;

    utlogctl_skel_get_script(&cdir, onoff) ;

    fprintf(stderr,"%s%s%s","skeleton ", onoff == LOGIN ? "login" : "logout"," script: ") ;

    for ( ; i < cdir.len ; i += strlen(cdir.s + i) + 1)

        fprintf(stderr,"%s ", cdir.s + i) ;

    fprintf(stderr,"%s","\n") ;


}

int main(int argc, char const *const *argv)
{
    PROG = "utlogctl" ;
    ULOG_VERBOSITY = 1 ;

    uint8_t what = 0, narg = 0, slash = 0 ;
    size_t userlen = 0, rlen = 0 ;
    char const *user = 0, *script = 0 ;
    char *rscript = 0 ;

    if (getuid())
       ulog_die(ULOG_EXIT_USER,"you must be root to run this script") ;

    utools_sanitize_skel() ;

    utlogctl_parse_cmd(argc, argv, &what, &narg) ;

    if (LIST && what >= 3) {
        /*
         *
         * what == 3 > list skelin
         * what == 4 > list skelout
         *
         * */

        if (what < 4) {

            utlogctl_list_skel_script(LOGIN) ;

        } else {

            utlogctl_list_skel_script(LOGOUT) ;
        }
        return 0 ;
    }

    if (!argv[narg])
        ulog_die(ULOG_EXIT_USER, USAGE) ;

    user = argv[narg] ;
    userlen = strlen(user) ;

    narg++ ;

    if (!argv[narg] && (what == 1 || what == 2) && !LIST)
        ulog_die(ULOG_EXIT_USER, USAGE) ;

    size_t oolen = (what == 1 || what == 3) ? SCRIPT_LOGIN_LEN : SCRIPT_LOGOUT_LEN ;
    int onoff = (what == 1 || what == 3) ? LOGIN : LOGOUT ;

    if (LIST) {

        /**
         *
         * !what > list log{on,off}
         * what == 1 > list login
         * what == 2 > list logout
         *
         * */

        char dst[UTLOGD_SCRIPT_PATH_LEN + userlen + 1] ;
        memcpy(dst, UTLOGD_SCRIPT_PATH, UTLOGD_SCRIPT_PATH_LEN) ;
        memcpy(dst + UTLOGD_SCRIPT_PATH_LEN, user, userlen) ;
        dst[UTLOGD_SCRIPT_PATH_LEN + userlen] = 0 ;

        if (access(dst,F_OK) < 0)
            ulog_f_die(ULOG_EXIT_USER,"%s%s%s","user: ", user," is not activated") ;

        if (what < 2) {

            utlogctl_list_user_script(user, LOGIN) ;

        }

        if (what != 1) {
            utlogctl_list_user_script(user, LOGOUT) ;
        }
        return 0 ;
    }

    if (what) {

        if (what > 2) {

            script = user ;
            rscript = (char *)user ;
            rlen = userlen ;

        } else {

            script = argv[narg] ;
            rscript = (char *)argv[narg] ;
            rlen = strlen(rscript) ;
        }

        if (ACTIVATE) {

            if (rscript[0] != '/')
                ulog_die(ULOG_EXIT_USER,"scriptname must be an absolute path") ;

            slash = 1 ;
            rscript = strrchr(rscript,'/') ;
            rlen = strlen(rscript) - slash ;

        } else {

            if (rscript[0] == '/')
                ulog_die(ULOG_EXIT_USER,"scriptname must be without path") ;

        }

    }

    if (!what) {

        /** user */

        if (ACTIVATE) {

            utlogctl_user_add(user) ;
            ulog_f_info("%s%s", user," user activated successfully") ;

        } else {

            utlogctl_user_del(user) ;
            ulog_f_info("%s%s", user," user deactivated successfully") ;
        }

    } else if (what) {

        if (what < 3) {

            /** log{on,off} */

            char dst[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen + 1 + rlen + 1] ;

            utools_user_set_arraypath(dst,user,onoff) ;

            memcpy(dst + UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen + 1, rscript + slash, rlen) ;
            dst[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen + 1 + rlen] = 0 ;

            if (ACTIVATE) {

                utlogctl_sanitize_user(user) ;

                if (SYMLINK) {

                    if (symlink(script, dst) < 0)
                        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s%s%s", "unable to symlink: ",dst, "to: ", script) ;

                } else {

                    if (!utools_filecopy(script,dst))
                        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s%s%s","unable to copy: ",script," to: ", dst) ;
                }
                ulog_f_info("%s%s%s%s%s%s", "script ", user, "/", onoff == LOGIN ? "login/" : "logout/", rscript + slash,  " activated successfully") ;

            } else {

                if (unlink(dst) < 0)
                    ulog_f_diesys(ULOG_EXIT_SYS,"%s%s","unable to remove: ",dst) ;

                 ulog_f_info("%s%s%s%s%s%s", "script ", user, "/", onoff == LOGIN ? "login/" : "logout/", rscript + slash,  " deactivated successfully") ;
            }

        } else {

            /** skel{on,off} */

            char dst[UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + SCRIPT_LOGOUT_LEN + 1 + rlen + 1] ;

            utlogctl_skel_set_arraypath(dst,onoff) ;

            memcpy(dst + UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + oolen + 1, rscript + slash, rlen) ;
            dst[UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + oolen + 1 + rlen] = 0 ;

            if (ACTIVATE) {

                if (SYMLINK) {

                    if (symlink(script, dst) < 0)
                        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s%s%s", "unable to symlink: ",dst, "to: ", script) ;

                } else {

                    if (!utools_filecopy(script,dst))
                        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s%s%s","unable to copy: ",script," to: ", dst) ;
                }

                ulog_f_info("%s%s%s%s%s", "skeleton script ", "skel/", onoff == LOGIN ? "login/" : "logout/", rscript + slash,  " activated successfully") ;

            } else {

                if (unlink(dst) < 0)
                    ulog_f_diesys(ULOG_EXIT_SYS,"%s%s","unable to remove: ",dst) ;

                ulog_f_info("%s%s%s%s%s", "skeleton script ", "skel/", onoff == LOGIN ? "login/" : "logout/", rscript + slash,  " deactivated successfully") ;

            }

        }
    }

    return 0 ;
}
