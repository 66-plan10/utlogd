#include <stdint.h>
#include <sys/types.h>
#include <utmpx.h>
#include <sys/inotify.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <utlogd/config.h>
#include <utlogd/ulog.h>
#include <utlogd/upoll.h>
#include <utlogd/utools.h>
#include <utlogd/usig.h>
#include <utlogd/constants.h>

#define USAGE "utlogd [ -v verbosity ] [ -d notif ]"

char const *help_utlogd =
"utlogctl [ -h ] [ -v verbosity ] [ -d notif ]"
"\n"
"options :\n"
"   -h: print this help\n"
"   -v: increase/decrease verbosity\n"
"   -d: notify readiness on file descriptor notif\n"
;

uint32_t INOTIFY_MASK = IN_CLOSE_WRITE | IN_DELETE_SELF ;
char const *USCRIPT = 0 ;
char *PROG = 0 ;

void utlogd_parse_opts(int argc, char const *const *argv, int *notify)
{
    argc-- ;
    argv++ ;
    int narg = 0 ;
    for (; narg < argc ; narg++) {

        if (argv[narg][0] == '-') {

                switch (argv[narg][1]) {

                    case 'h' :

                    fprintf(stderr, help_utlogd) ;
                    _exit(0) ;

                    case 'v' :

                        if (argv[narg][2]) {

                            if (argv[narg][2] < 48 || argv[narg][2] > 57)
                                ulog_die(ULOG_EXIT_USER, "invalid value for verbosity option") ;

                            ULOG_VERBOSITY = strtol(argv[narg] + 2, 0, 10) ;

                        } else {

                            narg++ ;

                            if (!argv[narg])
                                ulog_die(ULOG_EXIT_USER,"missing value for verbosity") ;

                            if (argv[narg][0] < 48 || argv[narg][0] > 57)
                                ulog_die(ULOG_EXIT_USER, "invalid value for verbosity option") ;

                            ULOG_VERBOSITY = strtol(argv[narg], 0, 10);

                        }
                        break ;

                    case 'd' :

                        if (argv[narg][2]) {

                            if (argv[narg][2] < 48 || argv[narg][2] > 57)
                                ulog_die(ULOG_EXIT_USER, "invalid value for notify option") ;

                            *notify = strtol(argv[narg] + 2, 0, 10) ;

                        } else {

                            narg++ ;

                            if (!argv[narg])
                                ulog_die(ULOG_EXIT_USER,"missing value for notify") ;

                            if (argv[narg][0] < 48 || argv[narg][0] > 57)
                                ulog_die(ULOG_EXIT_USER, "invalid value for notify option") ;

                            *notify = strtol(argv[narg], 0, 10);

                        }

                        break ;

                    default:
                        ulog_die(ULOG_EXIT_USER, USAGE) ;

            }
        }
        else
            ulog_die(ULOG_EXIT_USER, USAGE) ;
    }
}

int main(int argc, char const *const *argv)
{
    ULOG_VERBOSITY = 1 ;

    int notif_fd = -1 ;
    PROG = "utlogd" ;

    utools_sanitize_skel() ;

    utlogd_parse_opts(argc, argv, &notif_fd) ;

    /** Store actually logged user to be
     * able to see any difference on events */
    NUSER_DB = utools_read_utmp_database(USER_ID_DB) ;

    if (notif_fd > 0) {

        if (notif_fd < 3)
            ulog_die(ULOG_EXIT_USER,"notification fd must be 3 or more") ;

        if (fcntl(notif_fd, F_GETFD) < 0)
            ulog_diesys(ULOG_EXIT_USER,"invalid notification fd") ;

    }

    int signal_fd = usig_set_signals() ;

    int inotify_fd = inotify_init() ;

    if (inotify_fd < 0)
        ulog_diesys(ULOG_EXIT_SYS,"unable to initialize inofity") ;

    if (inotify_add_watch(inotify_fd,_PATH_UTMP, INOTIFY_MASK) == -1)
        ulog_f_diesys(ULOG_EXIT_SYS,"unable to watch event at: %s", _PATH_UTMP) ;

    upoll_supervise(signal_fd, inotify_fd, &notif_fd) ;

    usig_selfpipe_finish() ;

    close(inotify_fd) ;

    return 0 ;
}
