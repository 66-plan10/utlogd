/*
 * uspawn.c
 *
 * Copyright (c) 2021 Eric Vidal <eric@obarun.org>
 *
 * All rights reserved.
 *
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution.
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

#include <utlogd/uspawn.h>
#include <utlogd/utools.h>
#include <utlogd/usig.h>

pid_t uspawn_spawn_script(char const *prog, char const *const *argv, char const *const *envp)
{

    pid_t pid ;
    int pe ;
    int p[2] ;
    if (pipe(p) < 0)
        return -1 ;

    if (utools_fd_coe(p[1]) < 0)
        goto epipe ;

    pid = fork() ;
    if (pid < 0)
        goto epipe ;

    if (!pid) {

        close(p[0]) ;

        sigset_t s ;
        sigemptyset(&s) ;
        sigprocmask(SIG_SETMASK, &s, 0) ;

        execve(prog, (char *const *)argv,(char *const *)envp) ;
        pe = errno ;
        utools_fd_write(p[1], (char *)&pe, sizeof(pe)) ;
        _exit(127) ;
    }

    close(p[1]) ;
    p[1] = utools_fd_read(p[0], (char *)&pe, sizeof(pe)) ;
    if (p[1] < 0) {
        close(p[0]) ;
        return 0 ;
    }
    close(p[0]) ;
    if (p[1] == sizeof(pe)) {
        usig_waitpid_nointr(pid, &p[0], 0) ;
        errno = pe ;
        return 0 ;
    }
    return pid ;

    epipe:
        close(p[0]) ;
        close(p[1]) ;
        return 0 ;
}
