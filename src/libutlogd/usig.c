/*
 * usig.c
 *
 * Copyright (c) 2021 Eric Vidal <eric@obarun.org>
 *
 * All rights reserved.
 *
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution.
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 *
 * These functions are largely inspired from the selfpipe_XXX skalibs
 * library (https://git.skarnet.org/cgi-bin/cgit.cgi/skalibs/) which
 * is under ISC License.
 */

#include <sys/signalfd.h>
#include <signal.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>

#include <utlogd/ulog.h>
#include <utlogd/utools.h>

sigset_t selfpipe_caught ;
int selfpipe_fd = -1 ;


int usig_selfpipe_init(void)
{
    if (selfpipe_fd >= 0)
        return (errno = EBUSY, -1) ;

    sigemptyset(&selfpipe_caught) ;
    selfpipe_fd = signalfd(-1, &selfpipe_caught, SFD_NONBLOCK| SFD_CLOEXEC) ;

    return selfpipe_fd ;
}

int usig_selfpipe_trap(int sig)
{

    sigset_t scopy = selfpipe_caught ;
    sigset_t old ;
    if (selfpipe_fd == -1)
        return (errno = EBADF, -1) ;

    if ((sigaddset(&scopy, sig) < 0) || (sigprocmask(SIG_BLOCK, &scopy, &old) < 0))
        return -1 ;

    if (signalfd(selfpipe_fd, &scopy, SFD_NONBLOCK | SFD_CLOEXEC) < 0) {

        int e = errno ;
        sigprocmask(SIG_SETMASK, &old, 0) ;
        errno = e ;
        return -1 ;
    }
    selfpipe_caught = scopy ;
    return 1 ;
}

int usig_selfpipe_read(void)
{
    struct signalfd_siginfo buf ;
    ssize_t fr = utools_fd_read(selfpipe_fd, (char *)&buf, sizeof(struct signalfd_siginfo)) ;
    if (fr < 0)
        return fr ;

    ssize_t r = utools_sanitize_read(fr) ;
    return (r <= 0) ? r : buf.ssi_signo ;

}

int usig_selfpipe_untrap (int sig)
{
    sigset_t scopy = selfpipe_caught ;
    sigset_t ublock ;

    if (selfpipe_fd == -1)
        return (errno = EBADF, -1) ;

    int r = sigismember(&selfpipe_caught, sig) ;

    if (r < 0)
        return -1 ;

    if (!r)
        return (errno = EINVAL, -1) ;

    if ((sigdelset(&scopy, sig) < 0) ||
        (signalfd(selfpipe_fd, &scopy, SFD_NONBLOCK | SFD_CLOEXEC) < 0))
        return -1 ;

    sigemptyset(&ublock) ;
    sigaddset(&ublock, sig) ;

    if (sigprocmask(SIG_UNBLOCK, &ublock, 0) < 0) {
        int e = errno ;
        signalfd(selfpipe_fd, &selfpipe_caught, SFD_NONBLOCK | SFD_CLOEXEC) ;
        errno = e ;
        return -1 ;
    }

    selfpipe_caught = scopy ;
    return 0 ;
}

void usig_selfpipe_finish(void)
{
    sigprocmask(SIG_UNBLOCK, &selfpipe_caught, 0) ;
    sigemptyset(&selfpipe_caught) ;
    close(selfpipe_fd) ;

}

int usig_compute_child_exit(int wstat)
{
    if (WIFEXITED(wstat))
        return WEXITSTATUS(wstat) ;

    if (WIFSIGNALED(wstat))
        return 128 + WTERMSIG(wstat) ;

    return 111 ;
}

pid_t usig_waitpid_nointr(pid_t pid, int *wstat, int flags)
{
    pid_t r ;

    do
        r = waitpid(pid, wstat, flags) ;

    while ((r == (pid_t)-1) && (errno == EINTR)) ;

    return r ;
}

int usig_sig_handler(void)
{
    int wstat ;
    pid_t pid ;

    int e = errno ;
    errno = 0 ;

    for (;;) {

        int sig = usig_selfpipe_read() ;

        ulog_f_trace("received signale: %i ",sig) ;

        switch(sig) {

            case -1 :
                ulog_die(ULOG_EXIT_SYS,"unable to usig_selfpipe_read") ;
            case 0 :
                goto end ;
            case SIGINT :
                _FALLTHROUGH_ ;
            case SIGTERM :
                _FALLTHROUGH_ ;
            case SIGQUIT :

                {
                    while ((pid = usig_waitpid_nointr(-1, &wstat, WNOHANG)) > 0) {

                        ulog_f_info("pid  ->  %i", pid) ;

                        if (!pid)
                            _exit(usig_compute_child_exit(wstat)) ;
                    }

                    errno = e ;

                    _exit(0) ;
                }
            default :
                ulog_warn("unexpected data in selfpipe") ;
        }
    }
    end:
    return 1 ;
}

int usig_set_signals(void)
{
    int fd = usig_selfpipe_init() ;

    if (fd < 0)
        ulog_diesys(ULOG_EXIT_SYS, "selfpipe_init") ;

    if (usig_selfpipe_trap(SIGTERM) < 0)
        ulog_diesys(ULOG_EXIT_SYS, "unable to sigaction: SIGTERM") ;

    if (usig_selfpipe_trap(SIGINT) < 0)
        ulog_diesys(ULOG_EXIT_SYS, "unable to sigaction: SIGINT") ;

    if (usig_selfpipe_trap(SIGQUIT) < 0)
        ulog_diesys(ULOG_EXIT_SYS, "unable to sigaction: SIGQUIT") ;

    return fd ;
}
