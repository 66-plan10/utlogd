/*
 * utools.c
 *
 * Copyright (c) 2021 Eric Vidal <eric@obarun.org>
 *
 * All rights reserved.
 *
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution.
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <sys/types.h>
#include <sys/stat.h>//mkdir
#include <pwd.h>
#include <errno.h>
#include <stdint.h>
#include <utmpx.h>
#include <signal.h>//kill
#include <string.h>
#include <fcntl.h>
#include <unistd.h> //read, write
#include <dirent.h>

#include <utlogd/config.h>
#include <utlogd/utools.h>
#include <utlogd/ulog.h>
#include <utlogd/constants.h>
#include <utlogd/config.h>

uid_t NUSER_DB ;
uid_t USER_ID_DB[MAX_USER] ;

#define error_isagain(e) (((e) == EAGAIN) || ((e) == EWOULDBLOCK))

size_t utools_byte_count (char const *s, size_t len, char b)
{
    size_t n = 0 ;
    while (len--) if (*s++ == b) n++ ;
    return n ;
}

ssize_t utools_sanitize_read (ssize_t r)
{
  return r == -1 ? error_isagain(errno) ? (errno = 0, 0) : -1 :
         !r ? (errno = EPIPE, -1) : r ;
}

ssize_t utools_fd_read(int fd, char *buf, size_t buflen)
{
    ssize_t r ;
    do {
        r = read(fd, buf, buflen) ;
    } while ((r == -1) && (errno == EINTR)) ;
    return r ;
}

ssize_t utools_fd_write(int fd, char *buf, size_t buflen)
{
    ssize_t r ;
    do {
        r = write(fd, buf, buflen) ;
    } while ((fd == -1) && (errno == EINTR)) ;
    return r ;
}


int utools_fd_coe(int fd)
{
    int flags = fcntl(fd, F_GETFD, 0) ;
    return flags < 0 ? flags : fcntl(fd, F_SETFD, flags | FD_CLOEXEC) ;
}

int utools_get_uidbyname(char const *name, uid_t *uid)
{
    int e = errno ;
    errno = 0 ;
    struct passwd *pw = getpwnam(name) ;
    if (!pw) {

        if (!errno) errno = ESRCH ;
        return -1 ;
    }
    *uid = pw->pw_uid ;
    errno = e ;
    return 1 ;
}

int utools_get_namebyuid(uid_t uid, char *name)
{
    int e = errno ;
    errno = 0 ;
    struct passwd *pw = getpwuid(uid);
    if (!pw) {

        if (!errno) errno = ESRCH ;
        return 0 ;
    }
    size_t namelen = strlen(pw->pw_name) ;
    if (namelen > 255) {
        ulog_f_warn("string lenght of: %s is superior at 255 -- truncates it.",pw->pw_name) ;
        namelen = 255 ;
    }
    memcpy(name, pw->pw_name, namelen) ;
    name[namelen] = 0 ;
    errno = e ;
    return 1 ;
}

int utools_get_gidbyname(char const *name,gid_t *gid)
{
    int e = errno ;
    errno = 0 ;
    struct passwd *pw = getpwnam(name) ;
    if (!pw)
    {
        if (!errno) errno = ESRCH ;
        return -1 ;
    }
    *gid = pw->pw_gid ;
    errno = e ;
    return 1 ;
}

static int utools_is_user_process(struct utmpx *u)
{
    return (u->ut_user[0] && u->ut_type == USER_PROCESS) ||  (u->ut_tv.tv_sec != 0) ;
}

static int utools_want_entry(struct utmpx *u)
{
    uint8_t r = utools_is_user_process(u) ;

    if ((r && (u->ut_pid) <= 0) || (kill (u->ut_pid,0) < 0 && errno == ESRCH))
        return 0 ;

    return 1 ;
}

uid_t utools_read_utmp_database(uid_t *array)
{
    if (access(_PATH_UTMP,F_OK) < 0)
        ulog_f_die(ULOG_EXIT_SYS,"unable to access to: %s",_PATH_UTMP) ;

    setutxent() ;

    uid_t u = 0, nuser = 0 ;

    for (;;)
    {
        struct utmpx *utx ;
        errno = 0 ;
        utx = getutxent() ;

        if (!utx) break ;

        if (utx->ut_type != USER_PROCESS)
            continue ;

        if (utools_want_entry(utx)) {

            if (utools_get_uidbyname(utx->ut_user,&u) == -1)
                ulog_f_diesys(ULOG_EXIT_SYS,"unable get uid of: %s",utx->ut_user) ;

            nuser++ ;
            array[0] = nuser ;
            array[nuser] = u ;
        }
    }
    endutxent() ;

    return nuser ;
}

int utools_need_action(uid_t nuser)
{
    if (nuser == NUSER_DB)
        return 0 ;
    else if (nuser > NUSER_DB)
        return LOGIN ;
    else
        return LOGOUT ;

    /** should never be reached */
    return -1 ;
}

void utools_find_user_change(uid_t *user, uid_t *nuser)
{
    uint8_t found = 0 ;
    uid_t apos = 0, bpos = 0, a, b, *puser, *pdb ;
    uid_t nuser_to_handle = 0 ;
    uid_t user_to_handle[MAX_USER] ;

    if ((*nuser) < NUSER_DB) {

        a = NUSER_DB ;
        b = (*nuser) ;
        puser = USER_ID_DB ;
        pdb = user ;

    } else {

        a = (*nuser) ;
        b = NUSER_DB ;
        puser = user ;
        pdb = USER_ID_DB ;
    }

    for (; apos < a ; apos++) {

        found = 0 ;

        for (bpos = 0 ; bpos < b ; bpos++) {

            if (puser[apos + 1] == pdb[bpos + 1])
                found++ ;
        }
        if (!found) {

            nuser_to_handle++ ;
            user_to_handle[0] = nuser_to_handle ;
            user_to_handle[nuser_to_handle] = puser[apos + 1] ;
        }
    }

    for (apos = 0 ; apos < nuser_to_handle ; apos++)
        user[apos + 1] = user_to_handle[apos + 1] ;

    (*nuser) = nuser_to_handle ;
    user_to_handle[0] = (*nuser) ;

}

void utools_notify(int *fd)
{
    if ((*fd) >= 0) {

        utools_fd_write((*fd),"\n", 1) ;
        close((*fd)) ;
        (*fd) = -1 ;
    }
}

static int utools_auto_create(char const *dst, mode_t mode)
{
    if (mkdir(dst, mode) < 0)
        if (errno != EEXIST) return 0 ;
    return 1 ;
}

int utools_dir_create_parent(char const *dst, mode_t mode)
{
    size_t n = strlen(dst), i = 0 ;
    char tmp[n + 1] ;
    for (; i < n ; i++) {

        if ((dst[i] == '/') && i) {

            tmp[i] = 0 ;
            if (!utools_auto_create(tmp,mode)) return 0 ;
        }

        tmp[i] = dst[i] ;
    }

    if (!utools_auto_create(dst,mode))
        return 0 ;

    return 1 ;
}

void utools_sc_add_string(strchain_t *sc, char const *str, size_t len)
{
    if (len + sc->len > MAX_SC_ARRAY - 1) {
        ulog_f_warn("%s%s%s", "overflow -- ", str, " ignored") ;
        return ;
    }
    memcpy(sc->s + sc->len, str, len) ;
    sc->len += len ;
}

void utools_sc_init(strchain_t *sc)
{
    size_t i = 0 ;
    for ( ; i < MAX_SC_ARRAY ; i++)
        sc->s[i] = 0 ;

    sc->len = 0 ;
}

void utools_sc_sort(strchain_t *sc)
{
    if (!sc->len) return ;
    size_t sclen = sc->len, nel = utools_byte_count(sc->s,sc->len,'\0') , idx = 0, a = 0, b = 0, pos = 0 ;
    char names[nel][MAX_SC_ARRAY] ;
    char tmp[MAX_SC_ARRAY] ;

    for (; pos < sclen && idx < nel ; pos += strlen(sc->s + pos) + 1,idx++)
    {
        memcpy(names[idx],sc->s + pos,strlen(sc->s + pos)) ;
        names[idx][strlen(sc->s + pos)] = 0 ;
    }

    for (; a < nel - 1 ; a++)
    {
        for (b = a + 1 ; b < idx ; b++)
        {
            if (strcmp(names[a],names[b]) > 0)
            {
                memcpy(tmp,names[a], strlen(names[a])) ;
                tmp[strlen(names[a])] = 0 ;

                memcpy(names[a], names[b], strlen(names[b])) ;
                names[a][strlen(names[b])] = 0 ;

                memcpy(names[b], tmp, strlen(tmp)) ;
                names[b][strlen(tmp)] = 0 ;

            }
        }
    }

    utools_sc_init(sc) ;

    for (a = 0 ; a < nel ; a++)
        utools_sc_add_string(sc,names[a], strlen(names[a]) + 1) ;

}

int utools_get_dir(char const *dir, strchain_t *sc)
{

    struct dirent *d ;

    DIR *dr = opendir(dir) ;

    if (!dr)
        return 0 ;

    while ((d = readdir(dr)) != NULL) {

        if (d->d_name[0] == '.') {
            if (((d->d_name[1] == '.') && !d->d_name[2]) || !d->d_name[1]) {
                continue ;
            }
        }

        size_t len = strlen(d->d_name) ;
        utools_sc_add_string(sc, d->d_name, len + 1) ;
    }

    closedir(dr) ;

    return 1 ;

}

int utools_open_file(char const *file, unsigned int flags)
{
    int r ;
    do r = open(file, flags) ;
    while ((r == -1) && (errno == EINTR)) ;
    return r ;
}

int utools_open_file_mode(char const *file, unsigned int flags, mode_t mode)
{

    int r ;
    do r = open(file, flags, mode) ;
    while ((r == -1) && (errno == EINTR)) ;
    return r ;
}

int utools_filecopy(char const *src, char const *dst)
{
    struct stat st ;
    int rfd ; //read file
    int wfd ; //write file
    int remainw ; // N bytes remaining to be written
    int lastw ; // N bytes already written
    char *pb = 0 ; // ptr to buffer
    size_t bufflen = 0 ;

    if (lstat(src, &st) < 0)
        return 0 ;

    bufflen = st.st_size ;
    char buff[bufflen + 1] ;

    rfd = utools_open_file(src, O_RDONLY) ;
    if (rfd < 0)
        return 0 ;

    wfd = utools_open_file_mode(dst, O_WRONLY | O_CREAT | O_TRUNC, st.st_mode) ;
    if (wfd < 0)
        goto errr ;

    for(;;) {

        remainw = read(rfd, buff, bufflen) ;
        if (remainw > 0) {

            pb = buff ;

            while(remainw > 0) {

                lastw = write(wfd, pb, remainw) ;
                if (lastw < 0)
                    goto errw ;

                remainw -= lastw ;

                pb += lastw ;
            }
        } else if (!remainw) {
            // EOF
            break ;

        } else {
            // remainw < 0
            goto errw ;
        }
    }

    close(rfd) ;
    close(wfd) ;
    return 1 ;

    errw:
        close(wfd) ;
    errr:
        close(rfd) ;
        return 0 ;

}

void utools_sanitize_skel(void)
{
    uid_t uid ;
    gid_t gid ;

    char dst[UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + SCRIPT_LOGOUT_LEN + 1] ;

    if (utools_get_uidbyname(UTLOGD_RUNNER_USER,&uid) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s", "unable to get uid of: ",UTLOGD_RUNNER_USER) ;

    if (utools_get_gidbyname(UTLOGD_RUNNER_USER,&gid) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s", "unable to get gid of: ",UTLOGD_RUNNER_USER) ;

    memcpy(dst, UTLOGD_SCRIPT_PATH SCRIPT_SKEL, UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN) ;
    memcpy(dst + UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN, SCRIPT_LOGIN, SCRIPT_LOGIN_LEN) ;
    dst[UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + SCRIPT_LOGIN_LEN] = 0 ;

    ulog_f_trace("%s%s%s%lo","sanitize directory: ", dst," with mode: ", (unsigned long)0700) ;

    if (!utools_dir_create_parent(dst,0700))
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s","unable to create directory: ", dst) ;

    ulog_f_trace("%s%s%s%i:%i","chown: ", dst," with: ", (unsigned int) uid, (unsigned int) gid) ;

    if (chown(dst, uid, gid) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s", "unable to chown: ",dst) ;

    memcpy(dst + UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN, SCRIPT_LOGOUT, SCRIPT_LOGOUT_LEN) ;
    dst[UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN + SCRIPT_LOGOUT_LEN] = 0 ;

    ulog_f_trace("%s%s%s%lo","sanitize directory: ", dst," with mode: ", (unsigned long)0700) ;

    if (!utools_dir_create_parent(dst,0700))
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s","unable to create directory: ", dst) ;

    ulog_f_trace("%s%s%s%i:%i","chown: ", dst," with: ", (unsigned int) uid, (unsigned int) gid) ;

    if (chown(dst, uid, gid) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s", "unable to chown: ",dst) ;

    dst[UTLOGD_SCRIPT_PATH_LEN + SCRIPT_SKEL_LEN] = 0 ;

    ulog_f_trace("%s%s%s%i:%i","chown: ", dst," with: ", (unsigned int) uid, (unsigned int) gid) ;

    if (chown(dst, uid, gid) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s", "unable to chown: ",dst) ;

    dst[UTLOGD_SCRIPT_PATH_LEN] = 0 ;

    ulog_f_trace("%s%s%s%i:%i","chown: ", dst," with: ", (unsigned int) uid, (unsigned int) gid) ;

    if (chown(dst, uid, gid) < 0)
        ulog_f_diesys(ULOG_EXIT_SYS,"%s%s", "unable to chown: ",dst) ;

}

void utools_user_set_arraypath(char *array, char const *user, int onoff)
{
    char *oo = (onoff == LOGIN) ?  SCRIPT_LOGIN : SCRIPT_LOGOUT ;
    size_t oolen = (onoff == LOGIN) ? SCRIPT_LOGIN_LEN : SCRIPT_LOGOUT_LEN ;
    size_t userlen = strlen(user) ;

    memcpy(array, UTLOGD_SCRIPT_PATH,UTLOGD_SCRIPT_PATH_LEN) ;
    memcpy(array + UTLOGD_SCRIPT_PATH_LEN, user, userlen) ;
    array[UTLOGD_SCRIPT_PATH_LEN + userlen] = '/' ;
    memcpy(array + UTLOGD_SCRIPT_PATH_LEN + userlen + 1 , oo, oolen) ;
    array[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen] = '/' ;
}

void utools_user_get_script(strchain_t *cdir,char const *user, int onoff)
{
    size_t oolen = (onoff == LOGIN) ? SCRIPT_LOGIN_LEN : SCRIPT_LOGOUT_LEN ;
    size_t userlen = strlen(user) ;
    char dir[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen + 1] ;

    utools_user_set_arraypath(dir, user, onoff) ;
    dir[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen] = 0 ;

    if (!utools_get_dir(dir ,cdir))
        ulog_f_die(ULOG_EXIT_SYS, "%s%s","unable to get file from: ",dir) ;
}


