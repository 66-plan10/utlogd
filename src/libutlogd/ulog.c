/*
 * log.c
 *
 * Copyright (c) 2021 Eric Vidal <eric@obarun.org>
 *
 * All rights reserved.
 *
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution.
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <time.h>
#include <stdarg.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <utlogd/ulog.h>

uint8_t ULOG_VERBOSITY = 1 ;
uint8_t ULOG_SYS = ULOG_SYS_NO ;
struct timespec initime = { -1, -1 } ;

char const *ulog_msg[] = {

    [ULOG_LEVEL_ERROR] = "fatal:" ,
    [ULOG_LEVEL_INFO] = "info:" ,
    [ULOG_LEVEL_WARN] = "warning:" ,
    [ULOG_LEVEL_DEBUG] = "trace:" ,
} ;

void ulog_clock_init(void)
{
    if (initime.tv_sec >= 0)
        return ;

    clock_gettime(CLOCK_MONOTONIC, &initime) ;
}

static void ulog_time_compute(struct timespec *result, const struct timespec *now,const struct timespec *start)
{
    result->tv_sec = now->tv_sec - start->tv_sec ;
    result->tv_nsec = now->tv_nsec - start->tv_nsec ;

    if (result->tv_nsec < 0) {

        result->tv_sec-- ;
        result->tv_nsec += 1000000000 ;

    }
}

static void ulog_out(ulog_dbg_info_t *dbg_info, enum ulog_level level, const char *fmt, va_list args)
{
    char const *prefix = ulog_msg[level] ;
    struct timespec now = { 0 } ;

    clock_gettime(CLOCK_MONOTONIC, &now) ;
    ulog_time_compute(&now, &now, &initime) ;

    if (!strcmp("utlogd",PROG)) {
        fprintf(stderr,
                "%02d:%02d:%02d.%03ld ",
                (int)(now.tv_sec / 60 / 60),
                (int)(now.tv_sec / 60 % 60),
                (int)(now.tv_sec % 60),
                now.tv_nsec / 1000000) ;
    }

    if (ULOG_VERBOSITY >= 4)
        fprintf(stderr,"%s(%s: %s: %i): %s ", PROG,dbg_info->file,dbg_info->func,dbg_info->line, prefix) ;
    else
        fprintf(stderr,"%s: %s ", PROG, prefix) ;

    vfprintf(stderr, fmt, args) ;

    if (ULOG_SYS) {
        fprintf(stderr, ": ") ;
        fprintf(stderr,strerror(errno)) ;
    }

    fprintf(stderr, "\n") ;

}

void ulog_builder(ulog_dbg_info_t *dbg_info, enum ulog_level level, char const *fmt, ...)
{

    int e = errno ;

    va_list args ;

    if (level <= ULOG_VERBOSITY) {

        va_start(args, fmt) ;
        ulog_out(dbg_info, level, fmt, args) ;
        va_end(args) ;
    }

    errno = e ;
}

void ulog_die_builder (ulog_dbg_info_t *dbg_info, int e, enum ulog_level level, char const *fmt, ...)
{

    va_list args ;

    if (level <= ULOG_VERBOSITY) {

        va_start(args, fmt) ;
        ulog_out(dbg_info, level, fmt, args) ;
        va_end(args) ;
    }

    _exit(e) ;
}
