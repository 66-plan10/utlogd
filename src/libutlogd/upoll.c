/*
 * upoll.c
 *
 * Copyright (c) 2021 Eric Vidal <eric@obarun.org>
 *
 * All rights reserved.
 *
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution.
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <poll.h>
#include <errno.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <unistd.h> // environ
#include <sys/wait.h> //WNOHANG
#include <string.h>
#include <stdlib.h>

#include <utlogd/config.h>
#include <utlogd/constants.h>
#include <utlogd/utools.h>
#include <utlogd/ulog.h>
#include <utlogd/usig.h>
#include <utlogd/uspawn.h>

static void upoll_handle_event(const struct inotify_event *event)
{
    /** file was deleted */
    if (event->mask & IN_DELETE_SELF)
        ulog_f_die(ULOG_EXIT_SYS,"file %s was removed", _PATH_UTMP) ;

    pid_t pid ;
    int wstat ;
    uid_t user_db[MAX_USER], pos = 0 ;
    uid_t nuser = utools_read_utmp_database(user_db) ;

    int onoff = utools_need_action(nuser) ;

    if (onoff == -1)
        ulog_die(ULOG_EXIT_SYS,"inconsistent signal -- please make a bug report") ;

    if (!onoff) return ;

    size_t oolen = (onoff == LOGIN) ? SCRIPT_LOGIN_LEN : SCRIPT_LOGOUT_LEN ;

    utools_find_user_change(user_db,&nuser) ;

    for (; pos < nuser ; pos++) {

        char user[MAX_USER] ;
        size_t pos = 0, dirlen = 0 ;
        strchain_t cdir ;

        utools_sc_init(&cdir) ;

        utools_get_namebyuid(user_db[pos + 1],user) ;

        size_t userlen = strlen(user) ;

        ulog_f_info("user %s %s",user, onoff == LOGIN ? "login" : "logout") ;

        char dir[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen + 1] ;

        memcpy(dir,UTLOGD_SCRIPT_PATH, UTLOGD_SCRIPT_PATH_LEN) ;
        memcpy(dir + UTLOGD_SCRIPT_PATH_LEN, user, userlen) ;
        dir[UTLOGD_SCRIPT_PATH_LEN + userlen] = 0 ;

        if (access(dir,F_OK < 0)) {

            ulog_f_warn("%s%s",user," is not activated -- ignoring") ;
            continue ;
        }

        if (setenv("USER",user,1) < 0)
            ulog_f_warn("%s%s","unable to set environment with USER=",user)

        utools_user_set_arraypath(dir, user, onoff) ;
        dir[UTLOGD_SCRIPT_PATH_LEN + userlen + 1 + oolen] = 0 ;
        dirlen = strlen(dir) ;

        utools_user_get_script(&cdir,user,onoff) ;

        utools_sc_sort(&cdir) ;

        for (; pos < cdir.len ; pos += strlen(cdir.s + pos) + 1) {

            struct stat st ;
            uid_t uid = -1 ;
            gid_t gid = -1 ;
            char *sname = cdir.s + pos ;
            size_t snamelen = strlen(sname) ;
            char script[dirlen + 1 + snamelen + 1] ;

            memcpy(script, dir, dirlen) ;
            script[dirlen] = '/' ;
            memcpy(script + dirlen + 1, sname, snamelen) ;
            script[dirlen + 1 + snamelen] = 0 ;

            if ((utools_get_uidbyname(UTLOGD_RUNNER_USER, &uid) < 0) ||
                (utools_get_gidbyname(UTLOGD_RUNNER_USER, &gid) < 0))
                ulog_die(ULOG_EXIT_SYS,"unable to get uid and gid -- please make a bug report") ;

            if (!stat(script,&st)) {

                uid = st.st_uid ;
                gid = st.st_gid ;

            } else {

                ulog_f_warn("%s%s%s%s%s", "unable to stat: ", script," -- launch it with ",UTLOGD_RUNNER_USER," privileges") ;
            }

            ulog_f_trace("%s%i","setuid to: ",uid) ;
            if (setuid(uid) < 0) {
                ulog_f_warn("%s%s","unable to setuid -- ignoring script: ",script) ;
                continue ;
            }

            ulog_f_trace("%s%i","setgid to: ",gid) ;
            if (setgid(gid) < 0) {
                ulog_f_warn("%s%s","unable to setgid -- ignoring script: ",script) ;
                continue ;
            }

            ulog_f_info("%s%s","launch script: ",script) ;
            pid = uspawn_spawn_script(script,0,(char const *const *) environ) ;

            if (usig_waitpid_nointr(pid, &wstat, 0) < 0) {
                ulog_f_warn("unable to spawn: %s",script) ;
                continue ;
            }

            if (wstat) {
                ulog_f_warn("prog %s crashed with signal %i",script,usig_compute_child_exit(wstat)) ;
                continue ;
            }
        }
    }

    NUSER_DB = utools_read_utmp_database(USER_ID_DB) ;
}

static void upoll_read_events(int inotify_fd)
{

    ssize_t rlen = -1 ;
    const struct inotify_event *event ;

    for(;;) {
        errno = 0 ;

        unsigned int buflen ;
        if (ioctl(inotify_fd, FIONREAD, &buflen) < 0)
            ulog_diesys(ULOG_EXIT_SYS,"unable to ioctl") ;

        char buf[buflen + 1] __attribute__ ((aligned(__alignof__(struct inotify_event))));

        if (!buflen)
            break ;

        rlen = read(inotify_fd, buf, buflen) ;

        if (rlen == -1 && errno != EAGAIN)
            ulog_diesys(ULOG_EXIT_SYS,"unable to read events") ;

        if (rlen <= 0)
            break ;

        for (char *ptr = buf ; ptr < buf + rlen ; ptr += sizeof(struct inotify_event) + event->len) {

            event = (const struct inotify_event *) ptr ;

            if (event->mask & INOTIFY_MASK)
                upoll_handle_event(event) ;
        }
    }
}

void upoll_supervise(int signal_fd, int inotify_fd, int *notify_fd)
{
    int npoll ;
    nfds_t nfds = 2 ;
    struct pollfd poller_fds[2] ;

    poller_fds[0].fd = signal_fd ;
    poller_fds[0].events = POLLIN ;

    poller_fds[1].fd = inotify_fd ;
    poller_fds[1].events = POLLIN ;

    utools_notify(notify_fd) ;

    for(;;) {

        npoll = poll(poller_fds, nfds, -1) ;

        if (npoll == -1) {
            if (errno == EINTR)
                continue ;
            ulog_die(ULOG_EXIT_SYS,"unable to poll") ;
        }

        if (npoll > 0) {

            if (poller_fds[0].revents & (POLLIN|POLLHUP))
                if (!usig_sig_handler())
                    return ;

            if (poller_fds[1].revents & (POLLIN|POLLHUP))
                upoll_read_events(inotify_fd) ;

        }
    }
}
