/*
 * uspawn.h
 *
 * Copyright (c) 2021 Eric Vidal <eric@obarun.org>
 *
 * All rights reserved.
 *
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution.
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#ifndef UTLOGD_USPAWN_H
#define UTLOGD_USPAWN_H

#include <sys/types.h>

extern pid_t uspawn_spawn_script(char const *prog, char const *const *argv, char const *const *envp) ;

#endif
