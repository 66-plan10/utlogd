/*
 * utools.h
 *
 * Copyright (c) 2021 Eric Vidal <eric@obarun.org>
 *
 * All rights reserved.
 *
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution.
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#ifndef UTLOGD_UTOOLS_H
#define UTLOGD_UTOOLS_H

#include <sys/types.h>
#include <stdint.h>
#include <sys/inotify.h>

#include <utlogd/constants.h>

#if defined(__GNUC__) && __GNUC__ >= 7 || defined(__clang__) && __clang_major__ >= 12
#define _FALLTHROUGH_ __attribute__ ((fallthrough))
#else
#define _FALLTHROUGH_ ((void)0)
#endif

#define error_isagain(e) (((e) == EAGAIN) || ((e) == EWOULDBLOCK))

typedef struct strchain_s strchain_t, strchain_t_ref ;
struct strchain_s
{

    size_t len ;
    char s[MAX_SC_ARRAY] ;

} ;

extern char **environ ;

extern uid_t NUSER_DB ;
extern uid_t USER_ID_DB[MAX_USER] ;
extern uint32_t INOTIFY_MASK ;
extern char const *USCRIPT ;

extern size_t utools_byte_count (char const *s, size_t len, char b) ;
extern void utools_sanitize_skel(void) ;
extern ssize_t utools_sanitize_read (ssize_t r) ;
extern ssize_t utools_fd_read(int fd, char *buf, size_t buflen) ;
extern ssize_t utools_fd_write(int fd, char *buf, size_t buflen) ;
extern int utools_fd_coe(int fd) ;
extern int utools_get_uidbyname(char const *name, uid_t *uid) ;
extern int utools_get_namebyuid(uid_t uid, char *name) ;
extern int utools_get_gidbyname(char const *name,gid_t *gid) ;
extern uid_t utools_read_utmp_database(uid_t *array) ;
extern int utools_need_action(uid_t nuser) ;
extern void utools_find_user_change(uid_t *user, uid_t *nuser) ;
extern void utools_notify(int *fd) ;
extern int utools_dir_create_parent(char const *dst, mode_t mode) ;
extern void utools_sc_init(strchain_t *sc) ;
extern void utools_sc_add_string(strchain_t *sc, char const *str, size_t len) ;
extern void utools_sc_sort(strchain_t *sc) ;
#define utools_sc_add_string_g(sc, str) utools_sc_add_string(sc, (str), strlen(str))
extern int utools_get_dir(char const *dir, strchain_t *sc) ;
extern int utools_open_file(char const *file, unsigned int flags) ;
extern int utools_open_file_mode(char const *file, unsigned int flags, mode_t mode) ;
extern int utools_filecopy(char const *src, char const *dst) ;

extern void utools_user_set_arraypath(char *array, char const *user, int onoff) ;
extern void utools_user_get_script(strchain_t *cdir,char const *user, int onoff) ;
#endif
