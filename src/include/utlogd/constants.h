/*
 * constants.h
 *
 * Copyright (c) 2021 Eric Vidal <eric@obarun.org>
 *
 * All rights reserved.
 *
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution.
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#ifndef UTLOGD_CONSTANTS_H
#define UTLOGD_CONSTANTS_H

#define UTLOGD_RUNNER_USER "root"
#define UTLOGD_SCRIPT_PATH_LEN (sizeof UTLOGD_SCRIPT_PATH - 1)

#define SCRIPT_LOGIN "login"
#define SCRIPT_LOGIN_LEN (sizeof SCRIPT_LOGIN - 1)
#define SCRIPT_LOGOUT "logout"
#define SCRIPT_LOGOUT_LEN (sizeof SCRIPT_LOGOUT - 1)
#define SCRIPT_SKEL ".skel/"
#define SCRIPT_SKEL_LEN (sizeof SCRIPT_SKEL - 1)
#ifndef _UTMP_PATH_LEN
#define _UTMP_PATH_LEN (sizeof _PATH_UTMP + 1)
#endif
#define IBUF_LEN (sizeof(struct inotify_event) + /** length of the file to survey */ _UTMP_PATH_LEN + 1) * /* max number of event */10

#define LOGIN 2
#define LOGOUT 3
#define MAX_SC_ARRAY 512
#define MAX_USER 256


#endif
