/*
 * ulog.h
 *
 * Copyright (c) 2021 Eric Vidal <eric@obarun.org>
 *
 * All rights reserved.
 *
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution.
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#ifndef UTLOGD_ULOG_H
#define UTLOGD_ULOG_H

#include <stdarg.h>
#include <stdint.h>
#include <time.h>

#ifdef __GNUC__
#define attribute_printf(s, e) __attribute__((format(printf, s, e)))
#else
#define attribute_printf(s, e)
#endif

#define ULOG_SYS_NO 0
#define ULOG_SYS_YES 1
extern uint8_t ULOG_SYS ;

#define ULOG_EXIT_SYS 111
#define ULOG_EXIT_USER 110
extern uint8_t ULOG_VERBOSITY ;

enum ulog_level
{
    ULOG_LEVEL_ERROR = 0 ,
    ULOG_LEVEL_INFO ,
    ULOG_LEVEL_WARN ,
    ULOG_LEVEL_DEBUG

} ;

// Allow to see the file,function and the line
// at DEBUG/FLOW level
typedef struct ulog_dbg_info_s ulog_dbg_info_t ;
struct ulog_dbg_info_s {
    const char *file ;
    const char *func ;
    int line ;
} ;

#define ulog_dbg_init { .file = __FILE__, .func = __func__, .line = __LINE__ }

extern char *PROG ;
extern char const *ulog_msg[] ;

extern struct timespec initime ;

extern void ulog_init_clock(void) ;
extern void ulog_builder(ulog_dbg_info_t *dbg_info, enum ulog_level level, char const *fmt, ...) attribute_printf(3, 4) ;
extern void ulog_die_builder( ulog_dbg_info_t *dbg_info, int e, enum ulog_level level, char const *fmt, ...) attribute_printf(4, 5) ;

/**
 *
 * INFO
 *
 * */

#define ulog_info(str) do { \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_builder(&dbg_info, ULOG_LEVEL_INFO,"%s", str) ; \
} while(0) ;

#define ulog_f_info(fmt, ...) do { \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_builder(&dbg_info, ULOG_LEVEL_INFO, fmt, __VA_ARGS__) ; \
}while (0) ;

/**
 *
 * WARN
 *
 * */

#define ulog_warn(str) do { \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_builder(&dbg_info, ULOG_LEVEL_WARN, "%s", str) ; \
} while(0) ;

#define ulog_warnsys(str) do { \
    ULOG_SYS = ULOG_SYS_YES ; \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_builder(&dbg_info, ULOG_LEVEL_WARN, "%s", str) ; \
} while(0) ;

#define ulog_f_warn(fmt, ...) do { \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_builder(&dbg_info, ULOG_LEVEL_WARN, fmt, __VA_ARGS__) ; \
} while(0) ;

#define ulog_f_warnsys(fmt, ...) do { \
    ULOG_SYS = ULOG_SYS_YES ; \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_builder(&dbg_info, ULOG_LEVEL_WARN, fmt, __VA_ARGS__) ; \
} while(0) ;

/**
 *
 * TRACE
 *
 * */

#define ulog_trace(str) do { \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_builder(&dbg_info, ULOG_LEVEL_DEBUG, "%s", str) ; \
} while(0) ;

#define ulog_f_trace(fmt, ...) do { \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_builder(&dbg_info, ULOG_LEVEL_DEBUG, fmt, __VA_ARGS__) ; \
} while(0) ;

/**
 *
 * DIE
 *
 * */

#define ulog_die(err, str) do { \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_die_builder(&dbg_info, err, ULOG_LEVEL_ERROR, "%s", str) ; \
} while(0) ;

#define ulog_f_die(err, fmt, ...) do { \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_die_builder(&dbg_info, err, ULOG_LEVEL_ERROR, fmt, __VA_ARGS__) ; \
} while(0) ;

#define ulog_diesys(err, str) do { \
    ULOG_SYS = ULOG_SYS_YES ; \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_die_builder(&dbg_info, err, ULOG_LEVEL_ERROR, "%s", str) ; \
} while(0) ;

#define ulog_f_diesys(err, fmt, ...) do { \
    ULOG_SYS = ULOG_SYS_YES ; \
    ulog_dbg_info_t dbg_info = ulog_dbg_init ; \
    ulog_die_builder(&dbg_info, err, ULOG_LEVEL_ERROR, fmt, __VA_ARGS__) ; \
} while(0) ;

#endif
