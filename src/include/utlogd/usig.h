/*
 * usig.h
 *
 * Copyright (c) 2021 Eric Vidal <eric@obarun.org>
 *
 * All rights reserved.
 *
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution.
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#ifndef UTLOGD_USIG_H
#define UTLOGD_USIG_H

extern sigset_t selfpipe_caught ;
extern int selfpipe_fd ;

extern int usig_selfpipe_init(void) ;
extern int usig_selfpipe_trap(int sig) ;
extern int usig_selfpipe_read(void) ;
extern int usig_selfpipe_untrap (int sig) ;
extern void usig_selfpipe_finish(void) ;

extern int usig_set_signals(void) ;
extern int usig_sig_handler(void) ;
extern int usig_compute_child_exit(int wstat) ;
extern pid_t usig_waitpid_nointr(pid_t pid,int *wstat, int flags) ;

#endif
